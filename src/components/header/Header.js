import React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <div className="header">
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/mensagens">Mensagens</Link></li>
      </ul>
    </div>
  );
};

export default Header;
;