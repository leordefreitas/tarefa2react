import React, { useState } from 'react';
import './InputMensagem.css';
import axios from 'axios';


function InputMensagem() {

  // hooks state
  const [nome, setNome] = useState("");
  const [mensagem, setMensagem] = useState("");

  // body do post da api
  let body = {
    "message": {
      "name": nome,
      "message": mensagem
    }
  }

  // funcao que vai postar a mensagem
  const postData = () => {
    axios({
      method: 'post',
      url: 'https://treinamentoajax.herokuapp.com/messages',
      data: body
    })
  }

  return (
    <div className="input-mensagem">
      <input 
        placeholder="Nome" 
        type="text" 
        id="nome-ipt" 
        name="nome-ipt" 
        onChange={v => setNome(v.target.value)}
      />
      <textarea 
        placeholder="Digite aqui sua mensagem!" 
        id="mensagem-txt" 
        name="mensagem-txt" 
        rows="4" 
        cols="50"
        onChange={v => setMensagem(v.target.value)}
      />
      <button onClick={() => postData()}>Enviar</button>
    </div>
  );
};

export default InputMensagem;