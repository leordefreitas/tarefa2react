import React from 'react';
import './CadaMensagem.css';

function CadaMensagem(props) {
  const autor = props.autor;
  const conteudoMensagem = props.conteudoMensagem;

  return (
    <div className="cada-mensagem">
      <h2>{autor}</h2>
      <p>{conteudoMensagem}</p>
    </div>
  )
}

export default CadaMensagem;
