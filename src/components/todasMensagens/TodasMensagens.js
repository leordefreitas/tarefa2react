import React, { useState, useEffect } from 'react';
import './TodasMensagens.css';
import CadaMensagem from '../cadaMensagem/CadaMensagem';


function TodasMensagens(props) {
  const api = props.api;
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await api.get('messages');
      const responseArray = response.data;
      setMessages(responseArray);
    }
    fetchData()
  });  

  return (
    <div className="container-mensagens">
      {messages.map(e => (
        <CadaMensagem key={e.id} autor={e.name} conteudoMensagem={e.message} />
      ))}
    </div>
  );
}

export default TodasMensagens;