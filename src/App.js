import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from './pages/home/Home';
import Mensagens from './pages/mensagens/Mensagens';
import Header from './components/header/Header';


function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/"><Home /></Route>
        <Route exact path="/mensagens"><Mensagens /></Route>
      </Switch>
    </Router>
  );
}

export default App;
