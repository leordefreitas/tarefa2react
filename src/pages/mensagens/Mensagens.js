import React, { Fragment } from 'react';
import axios from 'axios';
import './Mensagens.css';
import TodasMensagens from '../../components/todasMensagens/TodasMensagens';
import InputMensagem from '../../components/inputMensagem/InputMensagem';

const api = axios.create({ 
  baseURL: 'https://treinamentoajax.herokuapp.com/' 
});

function Mensagens() {
  return (
    <Fragment>
      <InputMensagem api={api}/>
      <TodasMensagens api={api}/>
    </Fragment>
  );
}

export default Mensagens;
